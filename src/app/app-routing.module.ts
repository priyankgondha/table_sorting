import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CounterComponent } from './counter/counter.component';
import { HeaderComponent } from './header/header.component';
import { RestComponent } from './rest/rest.component';

const routes: Routes = [
  { path: 'rest', component: RestComponent },
  { path: 'counter', component: CounterComponent },
  { path: '', component: HeaderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
