import { Component, OnInit } from '@angular/core';
import { Observable, observable } from "rxjs";
import { Store, select } from "@ngrx/store";
import { increment, decrement, reset } from '../counter.actions';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  count: Observable<number>

  ngOnInit(): void {
  }

  constructor(private store: Store<{ count: number }>) {
    this.count = store.pipe(select('count'));
  }

  increment() {
    this.store.dispatch(increment())
  }
  decrement() {
    this.store.dispatch(decrement())
  }

  reset() {
    this.store.dispatch(reset())
  }

}
